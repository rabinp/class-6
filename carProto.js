/**
 * Car class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()
class Car {
    constructor(brand) {
        this.carname = brand;
        this.currentSpeed = 0;
    }
    toString() {
        return 'I have a ' + this.carname;
    }
    accelerate(){
        this.currentSpeed++
    }
    break(){
        this.currentSpeed--
    }
    
}

let myCar = new Car("Ford");

/**
 * ElectricCar class
 * @constructor
 * @param {String} model
 */

//  Create an instance, accelerate twice, brake once, and console log the instance.toString()

myCar = new Car('Volvo');
myCar.accelerate();
myCar.accelerate();
myCar.brake();
myCar.toString();
console.log(myCar.toString);
